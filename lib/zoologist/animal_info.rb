require "zoologist/animal"

module Zoologist
  class AnimalInfo
    def self.provide_info animal
      case animal.species
      when "gnu" then "The best animal in existence."
      when "penguin" then "A bird-like creature that is fine in both freezing and hot temperatures."
      when "snake" then "Did you mean 'snek'?"
      when "snek" then "sssssssssSSSSSSSSsssssssslithery."
      when "centipede" then "There is nothing at all wrong with centipedes."
      when "displacement beast" then "Displacement beasts are losers."
      else "This animal does not exist."
      end
    end
  end
end
