module Zoologist
  class Animal
    attr_reader :species
    
    def initialize species
      @species = species
    end

    def vocalize
      case species
      when "gnu" then "interjection!"
      when "snek" then "sssssssssSSSSSSSSssssssss!"
      when "penguin" then "noot noot!"
      when "centipede" then "AAAUUUUGGGGHHH!"
      when "displacement beast" then "arf!"
      else "*#{species} noises*"
      end
    end
  end
end
